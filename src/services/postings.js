import axios from "axios";
const baseUrl = "/api/postings";

const getAll = async () => {
  const response = await axios.get(baseUrl);
  return response.data;
};

const update = (id, newObject) => {
  const request = axios.put(`${baseUrl}/id/${id}`, newObject);
  return request.then(response => response.data);
};

const search = async (queryObject) => {
  const response = await axios.post(`${baseUrl}/query-search`, queryObject)
  return response.data
}

export default {
  getAll,
  update,
  search
};