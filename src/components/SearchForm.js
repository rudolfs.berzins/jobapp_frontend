import React from "react";
import { Button, Form } from "semantic-ui-react";

const SearchForm = ({
  givenSearchHandle,
  givenSearchValue,
  givenSearchValueHandle
}) => {
  return (
    <Form onSubmit={givenSearchHandle}>
      <Form.Field>
        <label>Search:</label>
        <input value={givenSearchValue} onChange={givenSearchValueHandle} />
      </Form.Field>
      <Button type="submit">Submit</Button>
    </Form>
    // <div>
    //   <form onSubmit={givenSearchHandle}>
    //     <div>
    //       search:
    //       <input value={givenSearchValue} onChange={givenSearchValueHandle} />
    //     </div>
    //     <div>
    //       <button type="submit">Search</button>
    //     </div>
    //   </form>
    // </div>
  );
};

export default SearchForm;
