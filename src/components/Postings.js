import React from "react";
import ph_img from "../images/placeholder.jpg";
import cl_img from "../images/cv_lv.png";
import ji_img from "../images/jobdk1.png";
import { Button, Item } from "semantic-ui-react";

Date.prototype.addDays = function(days) {
  const date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};

const imageObj = {
  cv_lv: cl_img,
  jobindex: ji_img
};

const PostingItem = ({
  title,
  link,
  company,
  salary_numbers,
  location,
  start_date,
  end_date,
  liked,
  source,
  toggleLiked
}) => {
  let salary_text;
  if (salary_numbers !== null) {
    salary_text =
      salary_numbers.length === 2
        ? `Min - ${salary_numbers[0]}, Max - ${salary_numbers[1]}`
        : `Min - ${salary_numbers[0]}`;
  } else {
    salary_text = "Not Shown";
  }

  const label = liked ? "liked" : "neutral";

  const srcImg = source in imageObj ? imageObj[source] : ph_img;
  const startDateObject = new Date(start_date);
  const endDateObject = end_date
    ? new Date(end_date)
    : startDateObject.addDays(21);
  const startDateString = startDateObject.toISOString().split("T")[0];
  const endDateString = endDateObject.toISOString().split("T")[0];

  return (
    <Item>
      <Item.Image size="small" src={srcImg} />
      <Item.Content verticalAlign="middle">
        <Item.Header>
          <a href={link} target="_blank" rel="noopener noreferrer">
            {title}
          </a>
        </Item.Header>
        <Item.Description>
          Company: {company} <br />
          Salary: {salary_text} <br />
          Location: {location} <br />
          Posted: {startDateString} <br />
          Deadline: {endDateString} <br />
        </Item.Description>
        <Item.Extra>
          <Button floated="right" onClick={toggleLiked}>
            {label}
          </Button>
        </Item.Extra>
      </Item.Content>
    </Item>
  );
};

const Postings = ({ postings, toggleLiked }) => {
  return (
    <Item.Group>
      {postings.map(posting => (
        <PostingItem
          key={posting.id}
          title={posting.title}
          link={posting.link}
          company={posting.company}
          salary_numbers={posting.salary_numbers}
          location={posting.location}
          start_date={posting.start_date}
          end_date={posting.end_date}
          liked={posting.liked}
          source={posting.source}
          toggleLiked={() => toggleLiked(posting.id)}
        />
      ))}
    </Item.Group>
  );
};

export default Postings;
