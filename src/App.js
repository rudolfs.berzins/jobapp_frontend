import React, { useState, useEffect } from "react";
import { Button, Grid, Header, Pagination } from "semantic-ui-react";
import Postings from "./components/Postings";
import postingService from "./services/postings";
import SearchForm from "./components/SearchForm";

const App = () => {
  const [postings, setPostings] = useState([]);
  const [pagePostings, setPagePostings] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [activePage, setActivePage] = useState(1);
  const [showAll, setShowAll] = useState(true);
  const pagePostingCount = 10;

  const fetchPostings = async () => {
    const allPostings = await postingService.getAll();
    setPostings(allPostings);
    setPagePostings(allPostings.slice(0, pagePostingCount));
  };

  useEffect(() => {
    fetchPostings();
  }, []);

  const postingsToShow = showAll
    ? postings
    : postings.filter(posting => posting.liked === true);

  useEffect(() => {
    const lastIndex = activePage * pagePostingCount;
    const firstIndex = lastIndex - pagePostingCount;
    const newPagePostings = postingsToShow.slice(firstIndex, lastIndex);
    setPagePostings(newPagePostings);
  }, [postings, postingsToShow]);

  const handleSearchValue = event => {
    setSearchValue(event.target.value);
  };

  const handlePaginationChange = (event, pageInfo) => {
    const lastIndex = pageInfo.activePage * pagePostingCount;
    const firstIndex = lastIndex - pagePostingCount;
    const newPagePostings = postingsToShow.slice(firstIndex, lastIndex);
    setActivePage(pageInfo.activePage);
    setPagePostings(newPagePostings);
  };

  const performSearch = async event => {
    event.preventDefault();
    const newPostings = await postingService.search({ query: searchValue });
    setPostings(newPostings);
  };

  const toggleLiked = id => {
    const posting = postings.find(n => n.id === id);
    const changedPosting = { ...posting, liked: !posting.liked };

    postingService
      .update(id, changedPosting)
      .then(returnedPosting => {
        const posts = postings.map(posting =>
          posting.id !== id ? posting : returnedPosting
        );
        setPostings(posts);
      })
      .catch(error => {
        alert(`the posting '${posting.title}' was already deleted from server`);
        setPostings(postings.filter(n => n.id !== id));
      });
  };

  let pagination = <></>;
  if (postingsToShow.length > pagePostingCount) {
    pagination = (
      <Pagination
        activePage={activePage}
        onPageChange={handlePaginationChange}
        totalPages={Math.ceil(postingsToShow.length / pagePostingCount)}
      />
    );
  }

  return (
    <Grid centered columns={2}>
      <Grid.Column>
        <Header as="h1">Postings</Header>
        <SearchForm
          givenSearchHandle={performSearch}
          givenSearchValue={searchValue}
          givenSearchValueHandle={handleSearchValue}
        />
        <div>
          <Button onClick={() => setShowAll(!showAll)}>
            show {showAll ? "liked" : "all"}
          </Button>
        </div>
        <Postings postings={pagePostings} toggleLiked={toggleLiked} />
        {pagination}
      </Grid.Column>
    </Grid>
  );
};

export default App;
